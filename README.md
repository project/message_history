INTRODUCTION
------------

The Message History module keeps track of which message a user has read. It
marks messages as _new_ depending on the last time the user viewed it.


REQUIREMENTS
------------

This module requires the following modules:

 * History (included in Drupal core)
 * Message (https://www.drupal.org/project/message)


INSTALLATION
------------

Install as usual, see https://www.drupal.org/docs/8/extending-drupal-8/
installing-modules for further information.
